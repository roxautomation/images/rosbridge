# Rosbridge image

see [rosbridge-suite](https://github.com/RobotWebTools/rosbridge_suite)

Running image:

        docker run -p 9090:9090 registry.gitlab.com/roxautomation/images/rosbridge:latest
